# End With A BANG SAM (Serverless Application Model)

backend API + DynamoDB

## AWS Resources

1. AWS Lambda Functions
1. AWS API Gateway
1. DynamoDB

## CI/CD

This repo uses GitLab CI/CD to automatically deploy to AWS

###### CI/CD Env Env

The follow env var should exists

| Environment related entity | GitLab CI var | Note                                   |
| -------------------------- | ------------- | -------------------------------------- |
| S3 Bucket name             | \$S3_BUCKET   | S3 Bucket that store the source bundle |
| stack Name                 | \$STACK_NAME  | Name for the AWS Cloudformation stack  |

###### CI/CD Strategy

1. COMING
2. SOON
