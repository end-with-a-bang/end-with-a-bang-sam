"use strict";

const app = require("../../app.js");
const utils = require("../../utils.js");
const chai = require("chai");
const expect = chai.expect;
var event, context;

event = {
  queryStringParameters: null
};

describe("Tests index", () => {
  xit("verifies successful response", async () => {
    await app.getTweets(event, context, (err, result) => {
      expect(result).to.be.an("object");
      expect(result.statusCode).to.equal(200);
      expect(result.body).to.be.an("string");

      let response = JSON.parse(result.body);

      expect(response).to.be.an("object");
      expect(response.message).to.be.equal("hello world");
      expect(response.location).to.be.an("string");
    });
  });
});

describe("transfromDynamoDBObj", () => {
  const transfromDynamoDBObj = utils.transfromDynamoDBObj;
  it("should transform DynamoDB object", () => {
    const dynamoDBObj = {
      "is_retweet": {
        "BOOL": true
      },
      "fake_id": {
        "N": "999"
      },
      "created_at": {
        "S": "Tue Jun 27 11:23:35 +0000 2017"
      },
      "time_stamp_format": {
        "S": "ddd MMM DD HH:mm:ss ZZ YYYY"
      },
      "text": {
        "S": "RT @foxandfriends: President Trump officially nominates former Assistant Attorney General Christopher Wray to head the FBI https://t.co/ld7…"
      },
      "source": {
        "S": "Twitter for iPhone"
      },
      "id_str": {
        "S": "879661481360588800"
      },
    };
    const expected = {
      is_retweet: true,
      fake_id: 999,
      created_at: "Tue Jun 27 11:23:35 +0000 2017",
      time_stamp_format: "ddd MMM DD HH:mm:ss ZZ YYYY",
      text: "RT @foxandfriends: President Trump officially nominates former Assistant Attorney General Christopher Wray to head the FBI https://t.co/ld7…",
      source: "Twitter for iPhone",
      id_str: "879661481360588800",
    };
    expect(expected).to.be.deep.equal(transfromDynamoDBObj(dynamoDBObj));
  });
});

describe("isStrIdGreaterThanMaxId", () => {
  const isStrIdGreaterThanMaxId = utils.isStrIdGreaterThanMaxId;
  it("should handle str of different length", () => {
    const currentMaxId = "60";
    const idStr = "160";
    const expected = true;
    const actual = isStrIdGreaterThanMaxId(idStr, currentMaxId);
    expect(expected).to.be.equal(actual);
    expect(true).to.be.equal(isStrIdGreaterThanMaxId("100", "99"));
    expect(true).to.be.equal(isStrIdGreaterThanMaxId("1000", "999"));
    expect(false).to.be.equal(isStrIdGreaterThanMaxId("100", "1900"));
    expect(false).to.be.equal(isStrIdGreaterThanMaxId("799", "1001"));
    expect(false).to.be.equal(isStrIdGreaterThanMaxId("999177487517233157", "1079888205351145472"));
  });
  it("should handle str of same length", () => {
    const currentMaxId = "60";
    const idStr = "60";
    const expected = false;
    const actual = isStrIdGreaterThanMaxId(idStr, currentMaxId);
    expect(expected).to.be.equal(actual);
    expect(true).to.be.equal(isStrIdGreaterThanMaxId("1080177487517233157", "1079888205351145472"));
    expect(true).to.be.equal(isStrIdGreaterThanMaxId("1080129307446513666", "1080115181831761922"));
    expect(false).to.be.equal(isStrIdGreaterThanMaxId("1080115181831761922", "1080177487517233157"));
    expect(true).to.be.equal(isStrIdGreaterThanMaxId("1080115181831761922", "1079902957938925568"));
    expect(false).to.be.equal(isStrIdGreaterThanMaxId("1079902957938925568", "1080114559925567488"));
    expect(false).to.be.equal(isStrIdGreaterThanMaxId("1080109395357380613", "1080114559925567488"));
    expect(true).to.be.equal(isStrIdGreaterThanMaxId("1080114559925567488", "1080109395357380613"));
    expect(false).to.be.equal(isStrIdGreaterThanMaxId("1080088373451206656", "1080114559925567488"));
    expect(false).to.be.equal(isStrIdGreaterThanMaxId("1079902957938925568", "1080114559925567488"));
    expect(false).to.be.equal(isStrIdGreaterThanMaxId("1080109395357380613", "1080114559925567488"));
    expect(true).to.be.equal(isStrIdGreaterThanMaxId("1079902957938925568", "1079900120047603713"));
    expect(true).to.be.equal(isStrIdGreaterThanMaxId("900", "199"));
  });
});
