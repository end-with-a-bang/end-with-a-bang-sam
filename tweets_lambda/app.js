/* eslint no-console: 0 */
var AWS = require("aws-sdk");
var axios = require("axios");

var chunk = require("lodash.chunk");

var transfromDynamoDBObj = require("./utils").transfromDynamoDBObj;
var isStrIdGreaterThanMaxId = require("./utils").isStrIdGreaterThanMaxId;
var transformTweetToDynamoDBObj = require("./utils").transformTweetToDynamoDBObj;

AWS.config.update({
  region: process.env.REGION
});

const dynamodb = new AWS.DynamoDB();
const TRUMP_TWEET_API = "http://www.trumptwitterarchive.com/data/realdonaldtrump/2020.json";
let response;

/**
 * return the next fake_id in the table
 * @param {}
 * @returns {Number} the count of the DynamoDB Table
 */

const getNextFakeId = async () => {
  const tableName = process.env.TABME_NAME || "Tweets";
  const fallbackMaxId = 7847;
  const fallbackMaxIdStr = "1127181493166649344";
  const params = {
    TableName: tableName,
    Key: {
      "fake_id": {
        N: "-999"
      }
    }
  };
  const maxTweetInfo = await dynamodb.getItem(params).promise().then(r => {
    const next_fake_id = (r.Item && r.Item.max_fake_id && r.Item.max_fake_id.N) || fallbackMaxId;
    const max_id_str = (r.Item && r.Item.max_id_str && r.Item.max_id_str.S) || fallbackMaxIdStr;
    return {
      next_fake_id,
      max_id_str
    };
  }).catch(err => {
    console.log("unable to get item with fake_id -999");
    console.log(`err is ${JSON.stringify(err, null, 4)}`);
    return {
      next_fake_id: fallbackMaxId,
      max_id_str: fallbackMaxIdStr
    };
  });
  return maxTweetInfo;
};

/**
 * Given the current lowest fake_id a user has and the number of tweets requested,
 * return the query params for the BatchGetItem method
 * @param {}
 * @returns {Number} the count of the DynamoDB Table
 */
const getBatchGetItemQuery = (currentMinFakeId, count = 20) => {
  const tableName = process.env.TABME_NAME || "Tweets";
  const nextMaxFakeId = currentMinFakeId - 1;
  // const nexMinFakeId = nextMaxFakeId - count + 1;
  const fakeIds = new Array(count).fill("").map((_, index) => {
    return nextMaxFakeId - index;
  });
  const batchGetItemQueries = fakeIds.map(fakeId => {
    const params = {
      "fake_id": {
        "N": fakeId.toString()
      },
    };
    return params;
  });
  return {
    RequestItems: {
      [tableName]: {
        Keys: batchGetItemQueries
      }
    }
  };
};

/**
 * Given the current next_fake_id, a list of tweets to be inserted, and the position of the list in the chunk of tweets,
 * return the query params for the BatchWriteItem method
 * @param {}
 * @returns {Objet} the param object for the DynamoDB Table BatchWriteItem
 */
const getBatchWriteItemQuery = (tweetsToInsert, next_fake_id, chunkPosition) => {
  const tableName = process.env.TABME_NAME || "Tweets";
  const preparedTweets = tweetsToInsert.map((tweet, index) => {
    return transformTweetToDynamoDBObj(tweet, parseInt(next_fake_id, 10) + 25 * chunkPosition  + index);
  });

  return {
    RequestItems: {
      [tableName]: preparedTweets
    }
  };
};

/**
 * Given the new fake_id and new_max_id_str,
 * return the update params for the update method (for id: -999 item)
 * @param {}
 * @returns {Number} the count of the DynamoDB Table
 */
const updateMaxFakeIdItemQuery = (new_next_fake_id, new_max_id_str) => {
  const tableName = process.env.TABME_NAME || "Tweets";
  const params = {
    TableName: tableName,
    Key: {
      fake_id: {
        N: "-999"
      },
    },
    UpdateExpression: "set max_fake_id = :new_next_fake_id, max_id_str = :new_max_id_str",
    ExpressionAttributeValues: {
      ":new_next_fake_id": {
        N: new_next_fake_id.toString()
      },
      ":new_max_id_str": {
        S: new_max_id_str.toString()
      }
    }
  };
  return params;
};

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 * @param {string} event.resource - Resource path.
 * @param {string} event.path - Path parameter.
 * @param {string} event.httpMethod - Incoming request's method name.
 * @param {Object} event.headers - Incoming request headers.
 * @param {Object} event.queryStringParameters - query string parameters.
 * @param {Object} event.pathParameters - path parameters.
 * @param {Object} event.stageVariables - Applicable stage variables.
 * @param {Object} event.requestContext - Request context, including authorizer-returned key-value pairs, requestId, sourceIp, etc.
 * @param {Object} event.body - A JSON string of the request payload.
 * @param {boolean} event.body.isBase64Encoded - A boolean flag to indicate if the applicable request payload is Base64-encode
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html
 * @param {Object} context
 * @param {string} context.logGroupName - Cloudwatch Log Group name
 * @param {string} context.logStreamName - Cloudwatch Log stream name.
 * @param {string} context.functionName - Lambda function name.
 * @param {string} context.memoryLimitInMB - Function memory.
 * @param {string} context.functionVersion - Function version identifier.
 * @param {function} context.getRemainingTimeInMillis - Time in milliseconds before function times out.
 * @param {string} context.awsRequestId - Lambda request ID.
 * @param {string} context.invokedFunctionArn - Function ARN.
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * @returns {boolean} object.isBase64Encoded - A boolean flag to indicate if the applicable payload is Base64-encode (binary support)
 * @returns {string} object.statusCode - HTTP Status Code to be returned to the client
 * @returns {Object} object.headers - HTTP Headers to be returned
 * @returns {Object} object.body - JSON Payload to be returned
 *
 */

// given the count of tweets desired and the tweet id, give the tweets after that given tweet id
exports.getTweets = async (event, /*context*/ ) => {
  console.log(event);
  const defaultCount = 20;
  let {
    count,
    afterId,
    hideRetweet = true
  } = event.queryStringParameters || {};
  count = parseInt(count, 10) || defaultCount;
  hideRetweet = hideRetweet === "false" ? false : true;
  let lastMaxFakeId;
  if (!afterId) {
    lastMaxFakeId = (await getNextFakeId()).next_fake_id;
  } else {
    lastMaxFakeId = parseInt(afterId, 10) || (await getNextFakeId()).next_fake_id;
  }

  try {
    let resultTweets = [];
    let resultCount = 0;
    let cursorMaxFakeId = lastMaxFakeId;
    while (resultCount < count) {

      const batchGetItemQueryParams = getBatchGetItemQuery(cursorMaxFakeId, count);
      const result = await dynamodb.batchGetItem(batchGetItemQueryParams).promise().then(r => {
        return r.Responses.Tweets;
      }).catch(err => {
        console.log("got error from dynamoDB");
        console.log(`err is ${JSON.stringify(err, null, 4)}`);
        throw err;
      });
      resultTweets = [].concat(resultTweets, result);
      resultCount = resultCount + (result.filter(t => hideRetweet ? !t.is_retweet.BOOL : true)).length;
      cursorMaxFakeId = resultTweets.map(t => t.fake_id.N).reduce((acc, curr)=>{
        return acc < curr ? acc : curr;
      });
    }


    const sortedTweets = resultTweets.sort((a, b) => {
      return parseInt(a.fake_id.N, 10) < parseInt(b.fake_id.N, 10) ? 1 : -1;
    });
    response = {
      statusCode: 200,
      body: JSON.stringify({
        tweets: sortedTweets.map(transfromDynamoDBObj)
      }),
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json"
      }
    };
  } catch (err) {
    console.log(err);
    return err;
  }

  return response;
};

/**
 * event doc: https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/CloudWatchEventsandEventPatterns.html
 *  @param {Object} event - CloudWatch Event Object
 *  @param {string} event.version - By default, this is set to 0 (zero) in all events.
 *  @param {string} event.id - A unique value is generated for every event. This can be helpful in tracing events as they move through rules to targets, and are processed.
 *  @param {string} event['detail-type'] - "Scheduled Event"
 *  @param {string} event.source - "aws.events"
 *  @param {string} event.account - account id
 *  @param {string} event.time - time stamp (in GMT)
 *  @param {string} event.region - aws region
 *  @param {Object} event.detail - "{}"
 *  @param {Array} event.resources - Array of resources ID for this event
 *  @param {Array[string]} event.resources[] - ARN for the event rule
 */
// can locally invoke
// sam local invoke "CheckTweetsFunction" -e tweets_lambda/events/schedule-event.json
exports.checkTweets = async (event, /*context*/ ) => {
  console.log("at checkTweets function");
  console.log(`event is ${JSON.stringify(event, null, 2)}`);

  const {
    next_fake_id,
    max_id_str
  } = await getNextFakeId();

  console.log(`next fake id is ${next_fake_id}`);
  const tweets = await axios.get(TRUMP_TWEET_API).then(r => {
    return r.data;
  });

  console.log(`first 10 tweets are ${JSON.stringify(tweets.slice(0, 10), null, 4)}`);
  let tweetsToInsert = [];
  for (let i = 0; i < tweets.length; i++) {
    if (isStrIdGreaterThanMaxId(tweets[i].id_str, max_id_str)){
      tweetsToInsert.push(tweets[i]);
    } else {
      break;
    }
  }
  tweetsToInsert = tweetsToInsert.reverse();

  if (!tweetsToInsert.length) {
    console.log("no new tweets avilable");
    return "no new tweets avilable";
  }
  const totalNumberOfTweets = tweetsToInsert.length;
  // chunk to be 25 tweets per subarray, because dynamodb only want 25 item per call
  tweetsToInsert = chunk(tweetsToInsert, 25);

  console.log(`tweetsToInsert is ${JSON.stringify(tweetsToInsert, null, 4)}`);
  const insertTweetsParams = tweetsToInsert.map((eachChunk, index) =>{
    return getBatchWriteItemQuery(eachChunk, next_fake_id, index);
  });

  console.log(`insertTweetsParams is ${JSON.stringify(insertTweetsParams, null, 2)}`);


  // now get the neweest tweet and its fake_id to get to the next_fake_id
  const lastChunkOfInsertTweetsParams = insertTweetsParams[insertTweetsParams.length-1];
  const lastChunk = lastChunkOfInsertTweetsParams["RequestItems"][process.env.TABME_NAME || "Tweets"];

  const newestTweet = lastChunk[lastChunk.length - 1];
  const new_next_fake_id = parseInt(newestTweet["PutRequest"]["Item"]["fake_id"]["N"], 10) + 1;
  const new_max_id_str = newestTweet["PutRequest"]["Item"]["id_str"]["S"];

  const updateQuery = updateMaxFakeIdItemQuery(new_next_fake_id, new_max_id_str);
  console.log(`updateQuery is ${JSON.stringify(updateQuery, null, 2)}`);



  await Promise.all(insertTweetsParams.map(insertTweetsParam => {
    return dynamodb.batchWriteItem(insertTweetsParam).promise().then(r => {
      console.log("batchWriteItem is completed");
      console.log(`r is ${JSON.stringify(r, null, 4)}`);
    }).catch(err => {
      console.log("batchWriteItem is failed");
      console.log(`err is ${JSON.stringify(err, null, 4)}`);
      throw err;
    });
  })).then(() => {
    return dynamodb.updateItem(updateQuery).promise();
  }).then(r => {
    console.log("updateItem for fake_id === -999 is completed");
    console.log(`r is ${JSON.stringify(r, null, 4)}`);
  }).then(() => {
    console.log(`number of new tweets inserted ${totalNumberOfTweets}`);
    console.log(`next fake_id is ${new_next_fake_id}`);
    console.log(`current id_str is ${new_max_id_str}`);
  }).then(() => {
    const commaSepDomains = process.env.DOMAINS;
    const domains = commaSepDomains.split(",");
    const webhookToken = process.env.WEBHOOK_TOKEN;
    return triggerWebhook(webhookToken, new_next_fake_id, domains);
  });
  return "Finished";
};

//TODO: inject the domains in gitlab CI
// add that into cfn template

/**
 *
 * @param {String} webhookToken token for invoking the webhook
 * @param {Number} newFakeId the max_fake_id in the table
 * @param {Array[string]} domains an array of domain of the server(s), no trailing /
 * @param {String} path the API endpoint path to the webhook
 */
const triggerWebhook = (webhookToken, newFakeId, domains, path = "/webhooks/update-fake-id") => {
  return domains.map(d => {
    return axios.post(`https://${d}${path}`, {
      next_fake_id: newFakeId
    }, {
      headers: {
        "x-webhook-token": webhookToken,
        "Content-Type": "application/json",
      }}).catch((error) => {
      if (error && error.response && error.response.status === 404){
        console.log(error.response.message);
      } else {
        console.log("have non 404 event. maybe the server isnt running");
      }
      throw error;
    }).then(() => {
      console.log(`webhook invoked at ${d}`);
    });
  });
};
