/**
 * Given an DynamoDB object,
 * transform the object return the json for API response
 * @param {}
 * @returns {object} the JSON for the API response
 */
const transfromDynamoDBObj = (dbObj) => {
  const json = {};
  json.fake_id = parseInt(dbObj.fake_id.N, 10);
  json.is_retweet = dbObj.is_retweet.BOOL;
  json.created_at = dbObj.created_at.S;
  json.time_stamp_format = dbObj.time_stamp_format.S;
  json.text = dbObj.text.S;
  json.source = dbObj.source.S;
  json.id_str = dbObj.id_str.S;
  if (dbObj.in_reply_to_user_id_str) {
    json.in_reply_to_user_id_str = dbObj.in_reply_to_user_id_str.S;
  }
  return json;
};

/**
 * Given a tweet's Id (in str) and the current max id (in start)
 * return if the tweet id is higher that current max id
 * @param {string} strId a tweet id in string
 * @param {string} currentMaxId the current max id in string
 * @returns {bool} if strId is a higher that currentMaxId
 */
const isStrIdGreaterThanMaxId = (idStr, currentMaxId) => {
  if (idStr.length !== currentMaxId.length) {
    return idStr.length > currentMaxId.length;
  }
  let result = false;
  for (let i = 0; i < idStr.length; i++) {
    if (idStr[i] > currentMaxId[i]) {
      result = true;
      break;
    } else if (idStr[i] < currentMaxId[i]) {
      result = false;
      break;
    }
  }
  return result;
};

/**
 * given a tweet, prepare it to be an item written to DynamoDB
 * @param {object} tweet a raw tweet object
 * @returns {object} a PutRequest object for DynamoDb
 */
const transformTweetToDynamoDBObj = (tweet, fakeId) => {
  const result = {
    PutRequest: {
      Item: {
        fake_id: {
          "N": fakeId.toString()
        },
        id_str: {
          "S": tweet.id_str
        },
        source: {
          "S": tweet.source
        },
        text: {
          "S": tweet.text
        },
        created_at: {
          "S": tweet.created_at
        },
        time_stamp_format: {
          "S": "ddd MMM DD HH:mm:ss ZZ YYYY"
        },
        is_retweet: {
          "BOOL": tweet.is_retweet
        },
      }
    }
  };
  if (tweet.in_reply_to_user_id_str) {
    result.PutRequest.Item.in_reply_to_user_id_str = {
      "S": tweet.in_reply_to_user_id_str
    };
  }
  return result;
};

exports.transfromDynamoDBObj = transfromDynamoDBObj;
exports.isStrIdGreaterThanMaxId = isStrIdGreaterThanMaxId;
exports.transformTweetToDynamoDBObj = transformTweetToDynamoDBObj;
