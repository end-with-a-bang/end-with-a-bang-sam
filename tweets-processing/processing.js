const fs = require("fs");
const path = require("path");
const transformTweetToDynamoDBObj = require("./utils").transformTweetToDynamoDBObj;

const tweets2017 = require("./tweets/tweets-2017.json");
const tweets2018 = require("./tweets/tweets-2018.json");

// check if processed-tweets folder exists

const destFolderExists = fs.existsSync(path.join(__dirname, "processed-tweets"));

if (!destFolderExists) {
  fs.mkdirSync(path.join(__dirname, "processed-tweets"));
} else {
  const files = fs.readdirSync(path.join(__dirname, "processed-tweets"));
  for (const file of files) {
    fs.unlinkSync(path.join(__dirname, "processed-tweets", file));
  }
}

const maxTweetPerPiece = process.env.MAX_TWEET_PER_PIECE || 20;
const tableName = process.env.TABLE_NAME || "Tweets";
// a piece that hold some obj and write out when it has (maxTweetPerPiece) object
let piece = [];
const tweets2017Reversed = tweets2017.reverse();
// for (let i = 0; i < tweets2017Reversed.length; i++) {
//   piece.push(transformTweetToDynamoDBObj(tweets2017Reversed[i], i));
//   if (piece.length >= maxTweetPerPiece || i === tweets2017Reversed.length - 1) {
//     fs.writeFileSync(path.join(__dirname, "processed-tweets", `processed-tweets-${i}.json`), JSON.stringify({
//       [tableName]: piece
//     }));
//     piece = [];
//   }
// }

const tweets2018Reversed = tweets2018.reverse();
for (let i = 0; i < tweets2018Reversed.length; i++) {
  piece.push(transformTweetToDynamoDBObj(tweets2018Reversed[i], i + tweets2017Reversed.length));
  if (piece.length >= maxTweetPerPiece || i === tweets2018Reversed.length - 1) {
    fs.writeFileSync(path.join(__dirname, "processed-tweets", `processed-tweets-${i + tweets2017Reversed.length}.json`), JSON.stringify({
      [tableName]: piece
    }));
    piece = [];
  }
}
