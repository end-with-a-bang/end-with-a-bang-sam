/**
 * given a tweet, prepare it to be an item written to DynamoDB
 * @param {object} tweet a raw tweet object
 * @returns {object} a PutRequest object for DynamoDb
 */
const transformTweetToDynamoDBObj = (tweet, fakeId) => {
  const result = {
    PutRequest: {
      Item: {
        fake_id: {
          "N": fakeId.toString()
        },
        id_str: {
          "S": tweet.id_str
        },
        source: {
          "S": tweet.source
        },
        text: {
          "S": tweet.text
        },
        created_at: {
          "S": tweet.created_at
        },
        time_stamp_format: {
          "S": "ddd MMM DD HH:mm:ss ZZ YYYY"
        },
        is_retweet: {
          "BOOL": tweet.is_retweet
        },
      }
    }
  };
  if (tweet.in_reply_to_user_id_str) {
    result.PutRequest.Item.in_reply_to_user_id_str = {
      "S": tweet.in_reply_to_user_id_str
    };
  }
  return result;
};

exports.transformTweetToDynamoDBObj = transformTweetToDynamoDBObj;
