#!/bin/bash

# need to make sure aws cli is installed an configured

for file in ./processed-tweets/*.json; do
    aws dynamodb batch-write-item --request-items file://$file --debug;
    sleep 4;
done
